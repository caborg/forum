@echo off

if "%~1" == "build" goto build
if "%~1" == "clean" goto clean
if "%~1" == "run" goto run
echo Unknown command
goto :end

:build
  echo Building server...
  go build

  echo Building client...
  setlocal
  cd client
  call npm install
  call npm run build-only
  endlocal
goto end

:clean
  del /S /Q forum.exe
  del /S /Q client\dist
  del /S /Q client\node_modules
  rmdir /S /Q client\dist
  rmdir /S /Q client\node_modules
goto end

:run
  .\server.exe
goto end

:end
