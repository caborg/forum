package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
)

type UserListing struct {
	Id       int64
	Username string
}

type UserDetails struct {
	Id       int64
	Username string
}

func userSummary(res http.ResponseWriter, req *http.Request) {
	user := UserDetails{
		Id:       1,
		Username: "dang",
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(res)
	encoder.Encode(user)
}

func listBlockedUsers(res http.ResponseWriter, req *http.Request) {
	users := []UserListing{
		{
			Id:       1,
			Username: "dang",
		},
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(res)
	encoder.Encode(users)
}

func listFriends(res http.ResponseWriter, req *http.Request) {
	users := []UserListing{
		{
			Id:       1,
			Username: "dang",
		},
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	encoder := json.NewEncoder(res)
	encoder.Encode(users)
}

func main() {
	// Account API
	http.HandleFunc("/api/me", userSummary)
	http.HandleFunc("/api/me/blocked", listBlockedUsers)
	http.HandleFunc("/api/me/friends", listFriends)

	// Frontend Provider
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "client/dist/index.html")
	})
	http.HandleFunc("/admin", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, "client/dist/index.html")
	})
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("client/dist/assets"))))

	// Start HTTP server.
	log.Println("Starting server...")
	wg := sync.WaitGroup{}
	wg.Add(1)
	httpServer := &http.Server{Addr: ":8080"}
	go func() {
		defer wg.Done()
		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe(): %v", err)
		}
	}()

	// Press the enter key to stop the server.
	fmt.Scanf("%s")
	log.Println("Stopping server...")
	if err := httpServer.Shutdown(context.TODO()); err != nil {
		panic(err)
	}
	wg.Wait()
	log.Println("Stopping database...")
}
